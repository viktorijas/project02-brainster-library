<?php
session_start();
if(!isset($_SESSION['user']) || !isset($_SESSION['admin']))
{
    header("Location:../view/index.php");
    die();
}
else
{
    require_once __DIR__ . "/layouts/db.php";
    require_once __DIR__ . "/layouts/header.php";
}

session_destroy();
header("Location:../view/index.php");
die();
