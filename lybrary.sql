-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 05, 2023 at 07:17 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lybrary`
--

-- --------------------------------------------------------

--
-- Table structure for table `author`
--

CREATE TABLE `author` (
  `id` int(10) UNSIGNED NOT NULL,
  `author` varchar(64) DEFAULT NULL,
  `soft_delete` smallint(5) DEFAULT 0,
  `biography` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `author`
--

INSERT INTO `author` (`id`, `author`, `soft_delete`, `biography`) VALUES
(1, 'Fyodor Dostoyevsky', 0, 'Born in Moscow in 1821, Dostoevsky was introduced to literature at an early age through fairy tales and legends, and through books by Russian and foreign authors.'),
(2, 'Leo Tolstoy', 0, 'Born to an aristocratic Russian family in 1828,[3] Tolstoy s notable works include the novels War and Peace (1869) and Anna Karenina (1878)'),
(23, 'napoleon hill', 0, 'Napoleon Hill was an American author in the area of the new thought movement who was one of the earliest producers of the modern genre of personal-success literature'),
(24, 'suzanne collins', 0, 'Suzanne Collins (born August 10, 1962) is an American author and television writer.');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(64) NOT NULL,
  `author_id` int(10) UNSIGNED NOT NULL,
  `year_publication` int(10) UNSIGNED NOT NULL,
  `pages` int(10) UNSIGNED NOT NULL,
  `cover` varchar(128) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `soft_delete` smallint(5) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `author_id`, `year_publication`, `pages`, `cover`, `category_id`, `soft_delete`) VALUES
(1, 'The Idiot', 1, 1869, 667, 'https://th.bing.com/th/id/R.a9bb3d51aeeb56d69bae13cbfb5d1cf4?rik=SPKavuJqfGPFzA&pid=ImgRaw&r=0', 2, 0),
(12, 'think and grow rich', 0, 1937, 233, 'https://images-na.ssl-images-amazon.com/images/S/compressed.photo.goodreads.com/books/1463241782i/30186948.jpg', 113, 0),
(13, 'war andpeace', 2, 1869, 1440, 'https://imgv2-2-f.scribdassets.com/img/word_document/373633159/original/cc9ec95aee/1591665969?v=1', 112, 0),
(14, 'mockingjay', 0, 2010, 398, 'https://th.bing.com/th/id/OIP.7yLeLANMo58b7BEMPTBGNQHaLa?pid=ImgDet&rs=1', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(64) DEFAULT NULL,
  `soft_delete` smallint(5) UNSIGNED DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category`, `soft_delete`) VALUES
(1, 'action & adventure', 0),
(2, 'classics', 0),
(112, 'fiction', 0),
(113, 'finance', 0);

-- --------------------------------------------------------

--
-- Table structure for table `personal_comments`
--

CREATE TABLE `personal_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `commentary` varchar(128) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `book_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `personal_comments`
--

INSERT INTO `personal_comments` (`id`, `commentary`, `user_id`, `book_id`) VALUES
(43, 'Up to page 105..', 13, 12);

-- --------------------------------------------------------

--
-- Table structure for table `public_comments`
--

CREATE TABLE `public_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `commentary` varchar(128) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status_comm` smallint(5) UNSIGNED DEFAULT 0,
  `book_id` int(10) UNSIGNED DEFAULT NULL,
  `soft_delete` smallint(5) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `public_comments`
--

INSERT INTO `public_comments` (`id`, `commentary`, `user_id`, `status_comm`, `book_id`, `soft_delete`) VALUES
(59, 'Nice book. ', 12, 1, 1, 0),
(60, 'I like this book!', 13, 0, 12, 0),
(61, 'love the book...', 14, 0, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` smallint(5) UNSIGNED DEFAULT 0,
  `user_name` varchar(64) DEFAULT NULL,
  `passkey` varchar(96) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `user_name`, `passkey`) VALUES
(10, 1, 'admin@example.com', '$2y$10$MrMmprySNEiPvgaubJDrOeHTk0TEgg9UyuzcY.xW6UF14OYpY3c6e'),
(11, 0, 'jane.doe@example.com', '$2y$10$jofvzB88lIpgty.5Q.p59u2l6Zh7fH7bNsRtdMoIJ6S.KdEsy5pwC'),
(12, 0, 'john.doe@example.com', '$2y$10$3nzTrNWHtY6lzy07iK4h9OfU3aHWEN9vOQcr5PMdE.UAOZcg3ZXLS'),
(13, 0, 'alice.l@example.com', '$2y$10$ZVzYCykrTURO.fI1zu4txeLddLPN91I4ALX1udQgNiPbzm5RcX7PO'),
(14, 0, 'ana.ana@example.com', '$2y$10$bV7vCdpLL7LrDHTjeh1OFu2h4yFZiRwjb7DMW.xTDUKX5lwvCm/lC'),
(15, 0, 'marko@example.com', '$2y$10$bzgAXlAY7VHZ0WIAtoTvxeGasqvh7yWg025BMm8Y2zQuf0quf6M.i');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `author`
--
ALTER TABLE `author`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author_id` (`author_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personal_comments`
--
ALTER TABLE `personal_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `book_id` (`book_id`);

--
-- Indexes for table `public_comments`
--
ALTER TABLE `public_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `book_id` (`book_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `author`
--
ALTER TABLE `author`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `personal_comments`
--
ALTER TABLE `personal_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `public_comments`
--
ALTER TABLE `public_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `books_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `personal_comments`
--
ALTER TABLE `personal_comments`
  ADD CONSTRAINT `personal_comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `personal_comments_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`);

--
-- Constraints for table `public_comments`
--
ALTER TABLE `public_comments`
  ADD CONSTRAINT `public_comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `public_comments_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
