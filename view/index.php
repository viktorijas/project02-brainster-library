<?php
require_once __DIR__ ."/../backEnd/layouts/header.php";
require_once __DIR__ . "/../backEnd/layouts/db.php";
?>

    <div class="container-fluid p-0 ">
        <div class=" bg-img m-0">
          <div class="col">
              <div class="waviy">
                <span style="--i:1">B</span>
                <span style="--i:2">R</span>
                <span style="--i:3">A</span>
                <span style="--i:4">I</span>
                <span style="--i:5">N</span>
                <span style="--i:6">S</span>
                <span style="--i:7">T</span>
                <span style="--i:8">E</span>
                <span style="--i:9">R </span>
                <span></span>
                <span></span>
                <span style="--i:10">L</span>
                <span style="--i:11">i</span>
                <span style="--i:12">b</span>
                <span style="--i:13">r</span>
                <span style="--i:14">a</span>
                <span style="--i:15">r</span>
                <span style="--i:16">y</span>
              </div>
          </div>
      </div>
            <?php
                if(isset($_SESSION['error'])) {
            ?>
                <div class="error h4 text-danger text-center mt-5"><?= $_SESSION['error']?></div>
            <?php 
                unset($_SESSION['error']);
              }
            ?>
        <div class="container-fluid">
          <div class="row">
            <div id="sideBar" class="col-md-3 d-flex flex-column bg-sidebar justify-content-center text-center mx-auto">
              <div id="checkDiv" class="col-sm-10 offset-sm-1 d-flex flex-column text-left text-capitalize">
              </div>
            </div>
            <div class="col-md-9 bg-color">
              <div id="containerCards" class="continer cards d-flex justify-content-center align-content-center flex-wrap">
                <div id="cardsDiv" class="d-flex justify-content-center align-content-center flex-wrap h-25"> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>

      <footer>
      <div class="container-fluid text-center text-white bg-color-footer-nav">
        <div class="row">
          <div class="col-12">
            <div name="quotation" class="font-bold d-flex flex-column col-md-8 offset-md-2 p-2 my-sm-2 border-bottom">
              <p id="quotation" class="my-1 mx-auto"></p>
              <span id="authorQuotation" class="tex-right ml-auto"></span>
            </div>
            <p class="p-2 my-2">
              Изработено со &#10084; од студентите на Brainster
            </p>
          </div>
        </div>
      </div>
    </footer>
    <?php
    require_once __DIR__ ."/../backEnd/layouts/footer.php";
    ?>
    <script src="./../js/general.js"></script>
    <script src="./../js/userSide.js"></script>
    <script src="./../js/signInSignUp.js"></script>
  </body>  
</html>
